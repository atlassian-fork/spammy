var config = eval(require("./config.json"));

var keys = config.keys;
var emails = config.emails;
var wts = config.email_send_each_run;

var ipsum = require('lorem-ipsum');

// Build POST String
var querystring = require('querystring');
var https = require('https');

var count = 0;
var currentAPIHolder = 0;
var apiLength = keys.length;

function nextAPI() {
	if (currentAPIHolder == apiLength - 1) {
		currentAPIHolder = 0;
	} else {
		currentAPIHolder++;
	}
	return keys[currentAPIHolder];
}

function sendElasticEmail(key, to, subject, body_text, body_html, from, fromName) {
	// Make sure to add your username and api_key below.
	var post_data = querystring.stringify({
		//'username' : 'dao.anh.duc.dh@gmail.com',
		//'api_key': 'b72c2b22-4408-4fe7-9349-3c9fbad4206c',
		'username': key,
		'api_key': key,
		'from': from,
		'from_name' : fromName,
		'to' : to,
		'subject' : subject,
		'body_html' : body_html,
		'body_text' : body_text
	});

	// Object of options.
	var post_options = {
		host: 'api.elasticemail.com',
		path: '/mailer/send',
		port: '443',
		method: 'POST',
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded',
			'Content-Length': post_data.length
		}
	};
	var result = '';
	// Create the request object.
	var post_req = https.request(post_options, function(res) {
		res.setEncoding('utf8');
		res.on('data', function (chunk) {
			result = chunk;
		});
		res.on('error', function (e) {
			result = 'Error: ' + e.message;
		});
	});

	// Post to Elastic Email
	post_req.write(post_data);
	post_req.end();
	return result;
}

setInterval(function () {
	emails.forEach(function (email) {
		var key = nextAPI();
		console.log(key);

		var subject = ipsum({units: 'sentences', count: 2});
		var body = ipsum({units: 'paragraphs', count: 5});

		var from = ipsum({units: 'word', count: 1});
		var domain = ipsum({units: 'word', count: 1});

		var fromEmail = from + "@" + domain + ".com";

		console.log(subject + "\n" + body + "\n" + email);

		if (++count == wts) {
			console.log("Sent " + count + ", happy to rest now");
			process.exit(1);
		}

		try {
			sendElasticEmail(key, email, subject, body, body, fromEmail, from);
		} catch(e) {
			console.log(e);
		}
		
	});
}, 3000);